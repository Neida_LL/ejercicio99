import { Component, OnInit,EventEmitter } from '@angular/core';
import { Comida1,Opciones1,Opciones2 } from "../../interfaces/comida.interface";
import { DataService } from "../../servicios/data.service";
@Component({
  selector: 'app-combobox',
  templateUrl: './combobox.component.html',
  styleUrls: ['./combobox.component.css']
})
export class ComboboxComponent implements OnInit {
  mensaje!:string;
  comidas!:Comida1[];
  comidita:Comida1={
    id:0,
    name:''
  }
  op1:Opciones1={
    id:0,
    id1:0,
    name:''
  }
  op2:Opciones2={
    id:0,
    id2:0,
    name:''
  }
  opciones1!:Opciones1[];
  opciones2!:Opciones2[];
  constructor(private dataService : DataService) { }

  ngOnInit(): void {
    this.comidas = this.dataService.getComida();
    //  console.log(this.comidas);
    // this.opciones2= this.dataService.getOpcion2();
    // console.log(this.opciones1,this.opciones2);
  }

  tipo1Select(idT1:any):void{
    this.opciones1 = this.dataService.getOpcion1().filter(e => e.id1==idT1.value);
    this.comidita= this.comidas[idT1.value];
  }
  tipo2Select(idT2:any):void{
    this.opciones2 = this.dataService.getOpcion2().filter(e => e.id2==idT2.value);
    // this.op1 = this.opciones1.find(e => e.id==idT2.value);
    this.opciones1.forEach(e =>{
      if (e.id==idT2.value) {
        this.op1=e;
      }
    })
  }

  tipo3Select(idT3:any):void{
    this.opciones2.forEach(e =>{
      if (e.id==idT3.value) {
        this.op2=e;
      }
    });
  }

  guardar(){
    // console.log(this.comidita);
    // console.log(this.op1);
    // console.log(this.op2);
    this.mensaje=`${this.comidita.name}\n${this.op1.name}\n${this.op2.name}`
  }

  Limpiar(){
    this.mensaje=' ';
  }
}
